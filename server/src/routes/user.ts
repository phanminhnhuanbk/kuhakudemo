import { Router } from 'express';
import * as jwt from 'jsonwebtoken';
import * as eJwt from 'express-jwt';
import * as ms from 'ms';
import { UserBusiness } from '../logic/UserBusiness';
import { join } from 'path';
export var userRouter = Router();
interface JsonResponse {
  status: Boolean;
  message?: string;
  [key: string]: any;
}
userRouter.post('/login', (req, res) => {
  if (req.body && req.body.username && req.body.password) {
    var business = new UserBusiness();
    let json: JsonResponse = { status: false };
    business.login({
      username: req.body.username,
      password: req.body.password
    }, (err, result) => {
      if (err) {
        json.status = false;
        json.message = err.message;
      }
      else {
        if (result == 0) {
          let token = jwt.sign(req.body, 'duckhan');
          json.status = true;
          json.token = token;
        }
        else {
          json.status = false;
          json.message = 'Username or password is wrong.';
        }
      }
      res.json(json);
    });
  }
  else {
    res.json({
      status: false,
      message: 'Username or password is empty.'
    }).end();
  }
});

userRouter.post('/register', (req, res) => {
  if (req.body && req.body.username && req.body.password) {
    var business = new UserBusiness();
    let json: JsonResponse = { status: false };

    business.addUser(
      {
        username: req.body.username,
        password: req.body.password
      }, err => {

        if (err) {
          json.status = false;
          json.message = err.message;
        }
        else {
          json.status = true;
        }
        res.json(json);
        res.end();

      });
  }
  else {
    res.json({
      status: false,
      message: 'Username or password is empty.'
    }).end();
  }
});

userRouter.get('/login', (req, res) => {
  res.end('Need login');
});
userRouter.get('/index', (req, res) => {
  if (req.user) {
    res.end('hello ' + req.user.username);
  }
  else {
    res.redirect('/user/login');
  }
});
