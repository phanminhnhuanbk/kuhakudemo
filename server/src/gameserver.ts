import * as socketio from 'socket.io';
import * as jwt from 'jsonwebtoken';
import { UnauthorizedError } from 'express-jwt';
import * as util from 'util';
interface ISocket extends SocketIO.Socket {
  [key: string]: any;
}
export class GameServer {
  public Server: SocketIO.Server;
  constructor(srv: any) {
    this.Server = socketio(srv);
    this.init();
    this.registerListener();
  }
  init() {
    this.Server.use((socket: ISocket, next) => {
      if (socket.handshake.query && socket.handshake.query.token) {
        jwt.verify(socket.handshake.query.token, 'duckhan', (err, decoded) => {
          if (err) {
            return next(new UnauthorizedError('Invalid token'));
          }
          else {
            socket.user = decoded;
            return next();
          }
        });
      }
      else {
        return next(new UnauthorizedError('Token not found'));
      }
    })
  }
  registerListener() {
      this.Server.on('connection',(socket:ISocket)=>{
        console.log('Connected '+ JSON.stringify(socket.user));
        let timer=setInterval(()=>{
          socket.emit('serverTime',new Date());
        },1000);
        socket.emit('hello',socket.user.username);
        socket.on('disconnect',soc=>{
          clearInterval(timer);
            console.log('Disconected: '+soc.user.username);
        });
      });
  }
}
