import * as express from 'express';
let app=express();
import * as bodyParser from 'body-parser'
app.use(bodyParser.json());//handle json request data
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
import * as jwt from 'express-jwt'
import * as expressSession from 'express-session'
import * as http from 'http';
let httpServer=new http.Server(app);
///handle json web token
app.use(jwt({
  secret: 'duckhan',
  credentialsRequired: false,
  getToken: function fromHeaderOrQuerystring (req) {
    // get token from query url
   if (req.query && req.query.token) {
      return req.query.token;
    }
    return null;
  }
}));
//midleway to catch invalid token exception
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send('invalid token...');
  }
});
import * as cookieParser from 'cookie-parser';
app.use(cookieParser());

import {userRouter} from './routes/user';
app.use('/user',userRouter);



import {GameServer} from './gameserver'
var gameServer=new GameServer(httpServer);

httpServer.listen(3000);
console.log('start listening on 3000');
