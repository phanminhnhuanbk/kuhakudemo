import * as fs from 'fs'
import * as path from 'path'
import * as Sequelize from 'sequelize'
import {UserModel} from './User'
interface DbConnection extends IAny{
  User:UserModel
}
interface IAny{
  [key:string]:any
}
export var db:IAny={};
const sequelize = new Sequelize(
  'kuhaku',
  'root',
  '123456',
  {
    host: 'mysql',
    dialect: 'mysql',
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    logging:false
  }  
)

const basename = path.basename(module.filename)
fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
  })
  .forEach(function(file) {
    const model = sequelize['import'](path.join(__dirname, file))
    // NOTE: you have to change from the original property notation to
    // index notation or tsc will complain about undefined property.
    db[model['name']] = model
  })

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
});

db['sequelize'] = sequelize;
db['Sequelize'] = Sequelize;
export default db;