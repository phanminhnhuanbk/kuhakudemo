import db from '../model/index';
import { User, UserAttributes } from '../model/User';
import { FindOptions } from 'sequelize';
import { isNullOrUndefined } from 'util';
export class UserBusiness {
  addUser(user: UserAttributes, callback: (err: Error) => void) {
    if (user) {
      db.User.findOne({
        where: {
          username: user.username
        }
      }).then(x => {
        if (isNullOrUndefined(x)) {
          db.User.create(user)
            .then(x => {
              if (x) {
                callback(null);
              }
              else {
                callback(new Error('Unknow error while add user'));
              }

            });
        }
        else {
          callback(new Error('User is exist.'));
        }
      }).catch(err => {
        callback(err);
      })
    }
    else {
      callback(new Error('User is null or undefined'));
    }
  }

  /**
   * Login method
   * @returns 
   * 0 if login sussess,
   * -1 if user not found,
   * -2 if password is wrong
   */

  login(user: UserAttributes, callback: (err, reuslt: number) => void) {
    var result = -3;
    if (user) {
      db.User.findOne({
        where: {
          username: user.username
        }
      })
        .then(u => {
          if (u) {
            //user exist
            if (u.password == user.password) {
              result = 0
            }
            else {
              result = -2;
            }
          }
          else {
            //user not exist
            result = -1;
          }
          callback(null, result);
        })
        .catch(err => {
          callback(err, -3);
        });
    }
    else {
      callback("User is null or undefined.", -3);
    }
  }
  getAllUser() {
    var users: Array<User>;
    return db.User.findAll();
  }
} 
