import socket from 'socket.io-client';
function subscribeTimer(callback){
  var client=socket();
  client.on('timer',function(time){
    if(callback){
      callback(time);
    }
  });
  client.emit('subscribeTimer',1000);
}
export {subscribeTimer};