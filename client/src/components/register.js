import React,{Component} from 'react';
export class Register extends Component{
  constructor(props){
    super(props);
    this.onRegisterClick=this.onRegisterClick.bind(this);
    this.callback=this.props.callback;
  }
  onRegisterClick(ev){
      let user={
        username:this.username.value,
        password:this.password.value
      }
      fetch('/user/register',{
        method:'post',
        headers:{
        'Content-Type':'application/json'  
        },
        body:JSON.stringify(user)
      }).then(res=>{
        return res.json();
      }).then(result=>{
          if(result.status){
              this.callback(result);
          }
          else{
            alert(result.message);
          }
      });
  }
  render(){
    return (
      <div>
        <div>
          <input type="text" ref={(username)=>this.username=username} placeholder="Username"/>
          <input type="password" ref={(pass)=>this.password=pass} placeholder="Password"/>
          <div>
            <input type="button" value="Register" onClick={this.onRegisterClick}/>
          </div>
        </div>
      </div>
    );
  }
}