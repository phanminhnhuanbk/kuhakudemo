import React, { Component } from 'react'
import socket from 'socket.io-client'
export class GameBoard extends Component {
  constructor() {
    super();
    this.OnConnectClick = this.OnConnectClick.bind(this);
    this.socket = null;
    this.state={
      username:null,
      serverTime:"dont have any data"
    };
  }
  OnConnectClick(ev) {
    alert("connecting");
    this.socket = socket({ query: { token: this.props.token }});
    this.socket.on('connection',()=>{
      console.log('Connected');
    });
    this.socket.connect();
    this.socket.on('serverTime',data=>{
      this.setState({serverTime:data});
    });
    this.socket.on('hello',username=>{
      this.setState({username:username});
    })
  }
  render() {
    let hello=null;
    if(this.state.username){
      hello=<div>Welcome, {this.state.username}</div>
    }
    return (
      <div>
        {hello}
        <div> Server time:
          {this.state.serverTime}
          </div>
        <div>Token:
          <br/>
          {this.props.token}
        </div>
        <input type="button" value="Connect" onClick={this.OnConnectClick} />
      </div>
    );
  }
}
